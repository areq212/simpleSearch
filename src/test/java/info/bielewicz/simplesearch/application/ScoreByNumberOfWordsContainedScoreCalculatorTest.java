package info.bielewicz.simplesearch.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import info.bielewicz.simplesearch.domain.Document;
import info.bielewicz.simplesearch.domain.DocumentRepository;
import info.bielewicz.simplesearch.domain.Score;
import info.bielewicz.simplesearch.domain.ScoreCalculator;
import info.bielewicz.simplesearch.domain.Word;

class ScoreByNumberOfWordsContainedScoreCalculatorTest {

	private final ScoreCalculator scoreCalculator = new ScoreByNumberOfWordsContainedScoreCalculator();
	private final DocumentRepository documentRepository = Mockito.mock(DocumentRepository.class);

	@Test
	void calculateScore_Should_Be100_When_AllWordsMatch() {
		// given
		final Word first = new Word("first");
		final Word second = new Word("second");
		final Word third = new Word("third");
		final Document document = new Document("document");
		final List<Word> queryWords = List.of(first, second, third);
		when(this.documentRepository.findDocumentsWithWordCount(first)).thenReturn(Map.of(document, 1L));
		when(this.documentRepository.findDocumentsWithWordCount(second)).thenReturn(Map.of(document, 2L));
		when(this.documentRepository.findDocumentsWithWordCount(third)).thenReturn(Map.of(document, 1L));

		// when
		final Map<Document, Score> documentScoreMap = this.scoreCalculator.calculateScore(queryWords, this.documentRepository);

		// then
		assertTrue(documentScoreMap.containsKey(document));
		assertEquals(100L, documentScoreMap.get(document).getValue());
	}

	@Test
	void calculateScore_Should_Be33_When_QueryHasThreeTimesSameWordWhileDocumentOnlyOneWordMatch() {
		// given
		final Word first = new Word("first");
		final Document document = new Document("document");
		final List<Word> queryWords = List.of(first, first, first);
		when(this.documentRepository.findDocumentsWithWordCount(first)).thenReturn(Map.of(document, 1L));

		// when
		final Map<Document, Score> documentScoreMap = this.scoreCalculator.calculateScore(queryWords, this.documentRepository);

		// then
		assertTrue(documentScoreMap.containsKey(document));
		assertEquals(33L, documentScoreMap.get(document).getValue());
	}

	@Test
	void calculateScore_Should_ReturnOnlyOneMatch_When_WithMultipleDocumentsOnlyOneHasMatchingWord() {
		// given
		final Word first = new Word("first");
		final Document firstDocument = new Document("firstDocument");
		final Document secondDocument = new Document("secondDocument");
		final List<Word> queryWords = List.of(first);
		when(this.documentRepository.findDocumentsWithWordCount(first)).thenReturn(Map.of(firstDocument, 1L, secondDocument, 0L));

		// when
		final Map<Document, Score> documentScoreMap = this.scoreCalculator.calculateScore(queryWords, this.documentRepository);

		// then
		assertTrue(documentScoreMap.containsKey(firstDocument));
		assertFalse(documentScoreMap.containsKey(secondDocument));
		assertEquals(100L, documentScoreMap.get(firstDocument).getValue());
	}
}