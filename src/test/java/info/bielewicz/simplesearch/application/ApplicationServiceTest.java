package info.bielewicz.simplesearch.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import info.bielewicz.simplesearch.domain.Document;
import info.bielewicz.simplesearch.domain.DocumentRepository;
import info.bielewicz.simplesearch.domain.Score;
import info.bielewicz.simplesearch.domain.ScoreCalculator;
import info.bielewicz.simplesearch.domain.Tokenizer;
import info.bielewicz.simplesearch.domain.Word;

class ApplicationServiceTest {

	private final DocumentRepository documentRepository = Mockito.mock(DocumentRepository.class);
	private final Tokenizer tokenizer = Mockito.mock(Tokenizer.class);
	private final ScoreCalculator scoreCalculator = Mockito.mock(ScoreCalculator.class);

	private final ApplicationService applicationService = new ApplicationService(this.documentRepository,
																				 this.tokenizer,
																				 this.scoreCalculator);

	@Test
	void addDocument_Should_AddDocumentToRepository() {
		// given
		final String documentName = "document";
		final Readable readable = Mockito.mock(Readable.class);
		final List<Word> wordList = List.of(new Word("Test"));
		when(this.tokenizer.tokenize(same(readable))).thenReturn(wordList);

		// when
		this.applicationService.addDocument(documentName, readable);

		// then
		verify(this.tokenizer).tokenize(same(readable));
		verifyNoMoreInteractions(this.tokenizer);

		verify(this.documentRepository).addDocument(
				argThat(document -> documentName.equals(document.getName())),
				eq(wordList));
		verifyNoMoreInteractions(this.documentRepository);
	}

	@Test
	void calculateScore() {
		// given
		final int numberOfScores = 5;
		final String documentName = "Document";
		final String query = "testQuery";
		final List<Word> wordList = List.of(new Word("ala"), new Word("ma"));

		final Map<Document, Score> documentScoreMap = LongStream
				.range(0, 8)
				.boxed()
				.collect(Collectors.toMap(i -> new Document(documentName.concat(String.valueOf(i))),
										  i -> new Score(i * 10)));

		when(this.tokenizer.tokenize(query)).thenReturn(wordList);
		when(this.scoreCalculator.calculateScore(same(wordList), same(this.documentRepository)))
				.thenReturn(documentScoreMap);

		// when
		final List<Map.Entry<String, Long>> sortedScores = this.applicationService.calculateScore(query,
																								  numberOfScores);

		// then
		assertEquals(numberOfScores, sortedScores.size());
		assertTrue(anyWithDocumentName(documentName.concat(String.valueOf(7)), sortedScores));
		assertTrue(sortedScores.contains(Map.entry(documentName.concat(String.valueOf(7)), 70L)));
		assertFalse(anyWithDocumentName(documentName.concat(String.valueOf(0)), sortedScores));

	}

	private static boolean anyWithDocumentName(
			final String documentName,
			final List<Map.Entry<String, Long>> sortedScores) {

		return sortedScores.stream().anyMatch(entry -> entry.getKey().equals(documentName));
	}
}