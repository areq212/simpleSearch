package info.bielewicz.simplesearch.application;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.StringReader;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import info.bielewicz.simplesearch.domain.DocumentRepository;
import info.bielewicz.simplesearch.domain.ScoreCalculator;
import info.bielewicz.simplesearch.domain.Tokenizer;

class ApplicationServiceIntegrationTest {

	private final DocumentRepository documentRepository = new ReverseIndexDocumentRepository();
	private final ScoreCalculator scoreCalculator = new ScoreByNumberOfWordsContainedScoreCalculator();
	private final Tokenizer tokenizer = new SplitByNonAlphanumericTokenizer();

	private final ApplicationService applicationService = new ApplicationService(this.documentRepository,
																				 this.tokenizer,
																				 this.scoreCalculator);

	@Test
	public void should_FindHighestScoredDocument() {
		// given
		this.applicationService.addDocument("firstDocument.txt", new StringReader(
				"So you think you are a geek, eh?  The first step is to admit to yourself\n"
				+ "your geekiness.  No matter what anyone says, geeks are people too; geeks\n"
				+ "have rights.  So take a deep breath and announce to the world that you are a\n"
				+ "geek.  Your courage will give you strength that will last you forever."
		));
		this.applicationService.addDocument("secondDocument.html", new StringReader(
				"<html lang=\"pl\" id=\"facebook\" class=\"\"><head><meta charset=\"utf-8\"><meta name=\"referrer\" "
				+ "content=\"origin-when-crossorigin\" id=\"meta_referrer\"><link rel=\"preload\" "
				+ "href=\"https://static.xx.fbcdn.net/rsrc.php/v3igW-4/y7/l/pl_PL/ZSJkveaFOGI.js?_nc_x=t2jO6AjO_u-\" "
				+ "as=\"script\" crossorigin=\"anonymous\"><link rel=\"preload\" href=\"https://static.xx.fbcdn"
				+ ".net/rsrc.php/v3/yo/r/BddImM399wN.js?_nc_x=t2jO6AjO_u-\" as=\"script\" "
				+ "crossorigin=\"anonymous\"><link rel=\"preload\" href=\"https://static.xx.fbcdn.net/rsrc"
				+ ".php/v3i--z4/y1/l/pl_PL/3tGHop43cJ3.js?_nc_x=t2jO6AjO_u-\" as=\"script\" "
				+ "crossorigin=\"anonymous\"><link rel=\"preload\" href=\"https://static.xx.fbcdn.net/rsrc"
				+ ".php/v3ijmi4/yB/l/pl_PL/6TQ-fYBR9mR.js?_nc_x=t2jO6AjO_u-\" as=\"script\" crossorigin=\"anonymous\">"
		));
		this.applicationService.addDocument("thirdDocument.exe", new StringReader(
				"......................................\n"
				+ "$$LOD$LOD$LOD$LOD$LOD$LOD$LOD$LOD$LOD$\n"
				+ "\n"
				+ "        ----------------------\n"
				+ "        :    Written to:    :\n"
				+ "        :                    :\n"
				+ "        :      K.A.O.S.      :\n"
				+ "        :                    :\n"
				+ "        :        as          :\n"
				+ "        :                    :\n"
				+ "        :    215-IBM-xxxx    :\n"
				+ "        :                    :"));

		// when
		final List<Map.Entry<String, Long>> resultList = this.applicationService.calculateScore(
				"script geekiness funny LOD as to to too A",
				2);

		// then
		assertEquals(2, resultList.size());
		assertEquals("firstDocument.txt", resultList.get(0).getKey());
		assertEquals(55, resultList.get(0).getValue());
		assertEquals("thirdDocument.exe", resultList.get(1).getKey());
		assertEquals(44, resultList.get(1).getValue());
	}

}