package info.bielewicz.simplesearch.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import info.bielewicz.simplesearch.domain.Tokenizer;
import info.bielewicz.simplesearch.domain.Word;

class SplitByNonAlphanumericTokenizerTest {

	private final Tokenizer tokenizer = new SplitByNonAlphanumericTokenizer();

	@Test
	public void tokenize_Should_SplitSentenceIntoWords() {
		// given
		final String value = "ala ma kota--kota:ma+-alĘ\n"
							 + "--------------------- New Movie Week! -------------\n"
							 + "January, 1994  [Etext #101]\n"
							 + "#kota #olę\n"
							 + "<TABLE WIDTH=100%>\n"
							 + "<TD BGCOLOR=#00FF00><FONT COLOR=#000000><B>Filename</B><BR></FONT></TD></TD>\n"
							 + "<TD BGCOLOR=#00DD00><FONT COLOR=#000000><B>Size</B><BR></FONT></TD></TD>\n"
							 + "<TD BGCOLOR=#00AA00><FONT COLOR=#000000><B>Description of the "
							 + "Textfile</B><BR></TD></TR>\n"
							 + "<tab indent=60 id=T><br>你吃饭了吗";

		// when
		final List<Word> tokenize = this.tokenizer.tokenize(value);


		// then
		assertEquals(66, tokenize.size());
		assertTrue(tokenize.contains(new Word("font")));
		assertTrue(tokenize.contains(new Word("br")));
		assertTrue(tokenize.contains(new Word("movie")));
		assertTrue(tokenize.contains(new Word("alę")));
		assertTrue(tokenize.contains(new Word("olę")));
		assertTrue(tokenize.contains(new Word("你吃饭了吗")));
	}

}