package info.bielewicz.simplesearch.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import info.bielewicz.simplesearch.domain.Document;
import info.bielewicz.simplesearch.domain.DocumentRepository;
import info.bielewicz.simplesearch.domain.Word;
import info.bielewicz.simplesearch.exception.DocumentAlreadySaved;

class ReverseIndexDocumentRepositoryTest {

	private final DocumentRepository repository = new ReverseIndexDocumentRepository();

	@Test
	public void addDocument_Should_ThrowException_When_DocumentWithSameNameWasAlreadyAdded() {
		// given
		final Document firstDocument = new Document("firstDocument");
		final List<Word> words = Stream.of("ala", "ma", "kota", "kot", "ma", "ale")
									   .map(Word::new)
									   .collect(Collectors.toList());
		this.repository.addDocument(firstDocument, words);

		// when
		assertThrows(DocumentAlreadySaved.class, () -> this.repository.addDocument(firstDocument, words));
	}

	@Test
	void findDocumentsWithWordCount_Should_ReturnWordProperCount_When_AddedDocumentContainsSpecifiedWord() {
		// given
		final Document document = new Document("TestDocumentName");
		final List<Word> words = Stream.of("ala", "ma", "kota", "kot", "ma", "ale")
										 .map(Word::new)
										 .collect(Collectors.toList());
		this.repository.addDocument(document, words);

		// when
		final Map<Document, Long> documentsWithWordCount = this.repository.findDocumentsWithWordCount(new Word("ma"));

		// then
		assertEquals(2, documentsWithWordCount.get(document));

	}
}