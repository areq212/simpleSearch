package info.bielewicz.simplesearch.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import info.bielewicz.simplesearch.exception.InvalidScore;

class ScoreTest {

	@Test
	public void score_Should_BeCrated_When_ValueIsInRange() {
		// given
		final Long testValue = 28L;

		// when
		final Score score = new Score(testValue);

		// then
		assertEquals(testValue, score.getValue());
	}

	@Test
	public void score_Should_ThrowException_When_ValueIsAboveRange() {
		// given
		final Long testValue = 132L;

		// when
		assertThrows(InvalidScore.class, () -> { new Score(testValue); });
	}

	@Test
	public void score_Should_ThrowException_When_ValueIsBelowRange() {
		// given
		final Long testValue = -1L;

		// when
		assertThrows(InvalidScore.class, () -> { new Score(testValue); });
	}

}