package info.bielewicz.simplesearch;

import info.bielewicz.simplesearch.application.ApplicationService;
import info.bielewicz.simplesearch.application.ReverseIndexDocumentRepository;
import info.bielewicz.simplesearch.application.ScoreByNumberOfWordsContainedScoreCalculator;
import info.bielewicz.simplesearch.application.SplitByNonAlphanumericTokenizer;
import info.bielewicz.simplesearch.domain.DocumentRepository;
import info.bielewicz.simplesearch.domain.ScoreCalculator;
import info.bielewicz.simplesearch.domain.Tokenizer;
import info.bielewicz.simplesearch.gui.SimpleSearchTUI;

public class Main {

	public static void main(final String[] args) {
		final DocumentRepository documentRepository = new ReverseIndexDocumentRepository();
		final Tokenizer tokenizer = new SplitByNonAlphanumericTokenizer();
		final ScoreCalculator scoreCalculator = new ScoreByNumberOfWordsContainedScoreCalculator();
		final ApplicationService applicationService = new ApplicationService(documentRepository, tokenizer, scoreCalculator);

		new SimpleSearchTUI(applicationService).start(args);
	}

}


