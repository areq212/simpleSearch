package info.bielewicz.simplesearch.application;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import info.bielewicz.simplesearch.domain.Document;
import info.bielewicz.simplesearch.domain.DocumentRepository;
import info.bielewicz.simplesearch.domain.Word;
import info.bielewicz.simplesearch.exception.DocumentAlreadySaved;

public class ReverseIndexDocumentRepository implements DocumentRepository {

	private final Set<Document> addedDocuments;
	private final Map<Word, Map<Document, Long>> wordsTable;

	public ReverseIndexDocumentRepository() {
		this.addedDocuments = new HashSet<>();
		this.wordsTable = new HashMap<>();
	}

	@Override
	public void addDocument(final Document document, final List<Word> words) {
		if (this.addedDocuments.contains(document)) {
			throw new DocumentAlreadySaved("Document with such name was already added");
		}
		this.addedDocuments.add(document);
		words.forEach(word -> this.addWord(document, word));
	}

	private void addWord(final Document document, final Word word) {
		final Map<Document, Long> documentsWithOccurrences = this.wordsTable.getOrDefault(word, new HashMap<>());
		documentsWithOccurrences.merge(document, 1L, Long::sum);
		this.wordsTable.put(word, documentsWithOccurrences);
	}

	@Override
	public Map<Document, Long> findDocumentsWithWordCount(final Word word) {
		return this.wordsTable.getOrDefault(word, Collections.emptyMap());
	}
}
