package info.bielewicz.simplesearch.application;

import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import info.bielewicz.simplesearch.domain.Tokenizer;
import info.bielewicz.simplesearch.domain.Word;

public class SplitByNonAlphanumericTokenizer implements Tokenizer {

	@Override
	public List<Word> tokenize(final Readable source) {
		try (final Scanner scanner = new Scanner(source)) {
			return scanner.useDelimiter("[^\\w\\p{L}]")
						  .tokens()
						  .filter(not(String::isBlank))
						  .map(String::trim)
						  .map(s -> s.toLowerCase(Locale.ENGLISH))
						  .map(Word::new)
						  .collect(toList());
		}
	}
}
