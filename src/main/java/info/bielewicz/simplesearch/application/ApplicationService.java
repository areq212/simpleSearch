package info.bielewicz.simplesearch.application;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import info.bielewicz.simplesearch.domain.Document;
import info.bielewicz.simplesearch.domain.DocumentRepository;
import info.bielewicz.simplesearch.domain.Score;
import info.bielewicz.simplesearch.domain.ScoreCalculator;
import info.bielewicz.simplesearch.domain.Tokenizer;

public class ApplicationService {

	private final DocumentRepository documentRepository;
	private final Tokenizer tokenizer;
	private final ScoreCalculator scoreCalculator;

	public ApplicationService(
			final DocumentRepository documentRepository,
			final Tokenizer tokenizer,
			final ScoreCalculator scoreCalculator) {

		this.documentRepository = documentRepository;
		this.tokenizer = tokenizer;
		this.scoreCalculator = scoreCalculator;
	}

	public void addDocument(final String documentName, final Readable documentContent) {
		this.documentRepository.addDocument(new Document(documentName), this.tokenizer.tokenize(documentContent));
	}

	public List<Map.Entry<String, Long>> calculateScore(final String query, final int countOfHighestScores) {
		final Map<Document, Score> listOfAllScores = this.scoreCalculator.calculateScore(
				this.tokenizer.tokenize(query),
				this.documentRepository);

		final List<Map.Entry<Document, Score>> listOfHighestScores = this.takeHighestScores(
				countOfHighestScores,
				listOfAllScores);

		return this.mapToResultType(listOfHighestScores);

	}

	private List<Map.Entry<Document, Score>> takeHighestScores(
			final int countOfHighestScores,
			final Map<Document, Score> results) {
		return results.entrySet().stream()
					  .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
					  .limit(countOfHighestScores)
					  .collect(Collectors.toList());
	}

	private List<Map.Entry<String, Long>> mapToResultType(final List<Map.Entry<Document, Score>> listOfScores) {
		return listOfScores.stream()
						   .map(entry -> Map.entry(entry.getKey().getName(), entry.getValue().getValue()))
						   .collect(Collectors.toList());
	}
}
