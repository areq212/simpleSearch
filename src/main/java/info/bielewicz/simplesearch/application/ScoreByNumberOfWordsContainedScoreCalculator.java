package info.bielewicz.simplesearch.application;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.summingLong;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import info.bielewicz.simplesearch.domain.Document;
import info.bielewicz.simplesearch.domain.DocumentRepository;
import info.bielewicz.simplesearch.domain.Score;
import info.bielewicz.simplesearch.domain.ScoreCalculator;
import info.bielewicz.simplesearch.domain.Word;

public class ScoreByNumberOfWordsContainedScoreCalculator implements ScoreCalculator {

	@Override
	public Map<Document, Score> calculateScore(
			final List<Word> queryWords,
			final DocumentRepository documentRepository) {

		return this.wordsWithCountInQuery(queryWords).entrySet().stream()
				   .flatMap(word -> this.documentsWithCountOfMatchingWord(word, documentRepository).entrySet().stream())
				   .collect(groupingBy(
						   Map.Entry::getKey,
						   collectingAndThen(
								   Collectors.summingLong(Map.Entry::getValue),
								   matchingWordsCount -> this.asPercentageOfAllQueryWords(matchingWordsCount, queryWords))));
	}

	private Score asPercentageOfAllQueryWords(final Long occurrences, final List<Word> queryWords) {
		return new Score(occurrences * 100L / queryWords.size());
	}

	private Map<Word, Long> wordsWithCountInQuery(final List<Word> queryWords) {
		return queryWords.stream().collect(groupingBy(Function.identity(), Collectors.counting()));
	}

	private Map<Document, Long> documentsWithCountOfMatchingWord(
			final Map.Entry<Word, Long> wordWithCount,
			final DocumentRepository documentRepository) {

		return documentRepository.findDocumentsWithWordCount(wordWithCount.getKey())
								 .entrySet()
								 .stream()
								 .filter(entry -> entry.getValue() > 0)
								 .collect(groupingBy(Map.Entry::getKey,
													 mapping(entry -> Math.min(wordWithCount.getValue(),
																			   entry.getValue()),
															 summingLong(value -> value))));


	}

}
