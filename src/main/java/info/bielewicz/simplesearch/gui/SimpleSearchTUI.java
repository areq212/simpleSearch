package info.bielewicz.simplesearch.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import info.bielewicz.simplesearch.application.ApplicationService;

public class SimpleSearchTUI {

	private final ApplicationService applicationService;

	private boolean isWorking = true;

	public SimpleSearchTUI(final ApplicationService applicationService) {
		this.applicationService = applicationService;
	}

	public void start(final String[] args) {
		if (args.length != 1) {
			System.err.println("Wrong number of parameters. Expected: Searcher DIRECTORY");
			return;
		}

		final String directoryPath = args[0];
		final File[] files = this.getFileFromDirectory(directoryPath);
		if (files == null) {
			System.err.println(directoryPath.concat(" is empty"));
			return;
		}
		final int indexFiles = this.indexFiles(files);
		System.out.println(String.valueOf(indexFiles).concat(" files read in directory ").concat(directoryPath));

		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
			while (this.isWorking) {
				this.readUserQuery(reader)
					.map(this::calculateScore)
					.ifPresent(this::printResults);
			}
		}
		catch (final IOException e) {
			System.err.println("Failed to read user input");
		}
	}

	private File[] getFileFromDirectory(final String directoryPath) {
		final File dir = new File(directoryPath);
		return dir.listFiles();
	}

	private Optional<String> readUserQuery(final BufferedReader reader) throws IOException {
		System.out.print("search> ");
		final String line = reader.readLine();

		if (line.isBlank()) {
			return Optional.empty();
		}

		if (":quit".equals(line)) {
			this.isWorking = false;
			return Optional.empty();
		}
		return Optional.of(line);
	}

	private List<Map.Entry<String, Long>> calculateScore(final String query) {
		return this.applicationService.calculateScore(query, 10);
	}

	private int indexFiles(final File[] directoryListing) {
		int indexedFiles = 0;
		for (final File file : directoryListing) {
			try {
				this.applicationService.addDocument(file.getName(), new FileReader(file));
				indexedFiles += 1;
			}
			catch (final FileNotFoundException e) {
				System.err.println("File not found: " + file.getName());
			}
		}
		return indexedFiles;
	}

	private void printResults(final List<Map.Entry<String, Long>> entries) {
		if (entries.isEmpty()) {
			System.out.println("no matches found");
			return;
		}

		for (final Map.Entry<String, Long> entry : entries) {
			System.out.println(entry.getKey().concat(" : ").concat(String.valueOf(entry.getValue()).concat("%")));
		}
	}

}
