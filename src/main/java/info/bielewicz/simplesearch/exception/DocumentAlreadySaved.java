package info.bielewicz.simplesearch.exception;

public class DocumentAlreadySaved extends RuntimeException {

	public DocumentAlreadySaved(final String message) {
		super(message);
	}
}
