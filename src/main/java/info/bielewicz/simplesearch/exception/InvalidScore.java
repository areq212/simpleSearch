package info.bielewicz.simplesearch.exception;

public class InvalidScore extends RuntimeException {

	public InvalidScore(final String message) {
		super(message);
	}
}
