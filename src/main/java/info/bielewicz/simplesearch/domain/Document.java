package info.bielewicz.simplesearch.domain;

import java.util.Objects;

public class Document {

	private final String name;

	public Document(final String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Document)) {
			return false;
		}
		final Document document = (Document) o;
		return Objects.equals(this.getName(), document.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getName());
	}
}
