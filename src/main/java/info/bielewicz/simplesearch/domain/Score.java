package info.bielewicz.simplesearch.domain;

import java.util.Objects;

import info.bielewicz.simplesearch.exception.InvalidScore;

public class Score implements Comparable<Score>{

	private final Long value;

	public Score(final Long value) {
		if (value < 0 || value > 100) {
			throw new InvalidScore("Score has to be as a value in range 0-100. Actual value: " + value);
		}
		this.value = value;
	}

	public Long getValue() {
		return this.value;
	}

	@Override
	public int compareTo(final Score o) {
		return this.value.compareTo(o.value);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Score)) {
			return false;
		}
		final Score score = (Score) o;
		return Objects.equals(this.getValue(), score.getValue());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getValue());
	}
}
