package info.bielewicz.simplesearch.domain;

import java.util.List;
import java.util.Map;

public interface DocumentRepository {

	void addDocument(Document document, List<Word> word);

	Map<Document, Long> findDocumentsWithWordCount(Word word);
}
