package info.bielewicz.simplesearch.domain;

import java.util.Objects;

public class Word {

	private final String value;

	public Word(final String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Word)) {
			return false;
		}
		final Word word = (Word) o;
		return Objects.equals(this.getValue(), word.getValue());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getValue());
	}

	@Override
	public String toString() {
		return "Word{" +
			   "value='" + this.value + '\'' +
			   '}';
	}
}
