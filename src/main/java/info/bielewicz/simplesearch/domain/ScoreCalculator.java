package info.bielewicz.simplesearch.domain;

import java.util.List;
import java.util.Map;

public interface ScoreCalculator {

	Map<Document, Score> calculateScore(final List<Word> queryWords, final DocumentRepository documentRepository);

}
