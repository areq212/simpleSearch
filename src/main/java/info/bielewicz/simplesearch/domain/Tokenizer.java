package info.bielewicz.simplesearch.domain;

import java.io.StringReader;
import java.util.List;

public interface Tokenizer {

	List<Word> tokenize(Readable source);

	default List<Word> tokenize(final String string) {
		return this.tokenize(new StringReader(string));
	}
}
