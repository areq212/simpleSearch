# Simple search engine
### Usage
To run program execute following command in your terminal/command line.
```
java -jar SimpleSearch.jar directoryContainingTextFiles
```

Right after startup program will index all file which are in specified directory, after that `search> ` prompt will be available.
To execute search, simply type query.
To quite program, type `:quit`.

Application treats every alphanumeric chain of characters as single word, any other character like `-,+\ /` etc. is treated as separator. Queries are case insensitive, so for `dog` and `DoG` result will be the same.

Score represents how many words from query exists in indexed files as percentage value. Duplicate values in query are also taken into account.

### Build
Project was build with Maven, to build fully executable JAR file with all dependencies execute.
```
mvn clean package
```
Runnable file can be found in `target` directory with name `simplesearch-1.0-jar-with-dependencies.jar`.

### Decisions
Main focus was put on separation domain logic from infrastructure.
 
I think goal was somehow achieved, as it should be quite easy to extend application with other access interfaces (GUI/Web). Current architecture and package structure is simple, just not to over engineer it.

Reverse indexing seems to be quite good choice in terms of efficiency.

### Improvements
* TUI definitely needs more love (and testing...)
* Some operations (score computation, tokenization) could be executed concurrently
* More mature architecture (Ports and Adapters?)
* New access possibility - REST API